<?php
ini_set ( "display_errors", "1");
ini_set ( "display_startup_errors", "1");
ini_set ( "html_errors", "1");

$pageTitle = 'example';
$gvId = 666;

if (isset($_GET['game'])) {
    $game = $_GET['game'];
    $pageTitle = $game;
    $gvId = 666;
    include(__DIR__.'/snippets/lg-template-local.inc');
} else {
    include(__DIR__.'/snippets/lg-main-page-local.inc');
}