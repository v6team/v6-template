# v6-template
Проект шаблона для игр

##запуск

файл index.php c параметрами

галвная
```
/index.php
```

игра
```
/index.php?game=your_game
```

авторизация игрока с заданным userId
```
/index.php?game=your_game&userid=12345
```

## настройка стилей

все обернуто в **main-wrapper** со стилем:

```css
#main-wrapper{
    width: 1000px;
    margin: 0 auto;
}
```
при необходимости ширину можно изменить увеличить, или уменьшить для vk

нижний блок **bottom-block** необходимо отцентрировать отступами:
```css
#bottom-block{
    margin: 0 190px 0 190px;
}
```

или, если игровое поле статичн, задав ширину:

```css
#bottom-block{
    width: 600px;
    margin: 0 auto;
}
```
