var _debugPanel = function() {
    var _client = window._client;
    _client.on('login', function (data) {
        console.log('main;', 'login', data.userId, data.userName);
    });

    _client.gameManager.on('game_start', function (data) {
        console.log('main;', 'game_start, room: ', data);
    });

    _client.gameManager.on('round_start', function (data) {
        console.log('main;', 'round_start, room: ', data);
    });

    _client.gameManager.on('turn', function (data) {
        console.log('main;', 'turn', data.turn, 'is your turn: ', data.user == _client.getPlayer().userId);
    });

    _client.gameManager.on('switch_player', function (data) {
        console.log('main;', 'switch_player', 'next: ', data, 'your next: ', data.userId == _client.getPlayer().userId);
    });

    _client.gameManager.on('event', function (data) {
        console.log('main;', 'event', data);
    });

    _client.gameManager.on('timeout', function (data) {
        console.log('main;', 'timeout', 'user: ', data.user, 'is your timeout: ', data.user == _client.getPlayer());
    });

    _client.gameManager.on('round_end', function (data) {
        console.log('main;', 'round_end', data, 'your win: ', data.winner == _client.getPlayer().userId);
    });

    _client.gameManager.on('game_leave', function (data) {
        console.log('main;', 'game_leave room:', data);
    });

    _client.gameManager.on('game_load', function (data) {
        console.log('main;', 'game_loaded, game history:', data);
    });

    _client.gameManager.on('take_back', function (data) {
        console.log('main;', 'take_back user: ', data.user, 'history:', data.history);
    });

    _client.gameManager.on('time', function (data) {
        var html = (data.user ? ((data.user.isPlayer ? 'Ваш ход' : 'Ход соперника')) : 'Time: ') + ' ' + data.userTimeFormat;
        html += '<br>';
        html += 'мое общее время: ' + data.userTotalTime.timeFormat;
        html += '<br>';
        html += 'Общее время: ' + data.totalTime.timeFormat;
        html += 'Время раунда: ' + data.roundTime.timeFormat;
        $('#time').html(html)
    });


    _client.gameManager.on('focus', function (data) {
        //console.log('main;', 'user changed window focus, window has focus:', data.windowHasFocus, ' user: ', data.user);
    });


    _client.historyManager.on('game_load', function (game) {
        console.log('main;', 'history game loaded, game:', game);
    });

    _client.on('show_profile', function (data) {
        console.log('main;', 'show_profile user:', data);
    });

    _client.on('settings_changed', function (data) {
        console.log('main;', 'settings_changed property:', data);
    });

    _client.on('settings_saved', function (data) {
        console.log('main;', 'settings_changed settings:', data);
    });


// send events buttons example
    _generateEndGameBtn();

    function _generateEndGameBtn() {
        var bdiv = $('<div>');
        bdiv.addClass('v6-buttons');
        $('body').append(bdiv);

        var div = $('<div>');
        div.attr('id', 'settingsGameButton');
        div.html('<span>Настройки</span>');
        div.on('click', function () {
            window._client.viewsManager.showSettings();
        });
        bdiv.append(div);

        var div = $('<div>');
        div.attr('id', 'endGameButton');
        div.html('<span>Выйти из игры</span>');
        div.on('click', function () {
            window._client.gameManager.leaveGame();
        });
        bdiv.append(div);

        div = $('<div>');
        div.attr('id', 'drawButton');
        div.html('<span>Предложить ничью</span>');
        div.on('click', function () {
            window._client.gameManager.sendDraw();
        });
        bdiv.append(div);

        div = $('<div>');
        div.attr('id', 'drawButton');
        div.html('<span>Сдаться</span>');
        div.on('click', function () {
            window._client.gameManager.sendThrow();
        });
        bdiv.append(div);

        div = $('<div>');
        div.attr('id', 'winButton');
        div.html('<span>Победный ход</span>');
        div.on('click', function () {
            window._client.gameManager.sendTurn({result: 1});
        });
        bdiv.append(div);

        div = $('<div>');
        div.attr('id', 'ratingButton');
        div.html('<span>Показать рейтинг</span>');
        div.on('click', function () {
            window._client.ratingManager.getRatings();
        });
        bdiv.append(div);

        div = $('<div>');
        div.attr('id', 'historyButton');
        div.html('<span>Показать историю</span>');
        div.on('click', function () {
            window._client.historyManager.getHistory(false, false, false);
        });
        bdiv.append(div);

        div = $('<div>');
        div.html('<span>Передать ход</span>');
        div.on('click', function () {
            window._client.gameManager.sendTurn({'switch': true});
        });
        bdiv.append(div);

        div = $('<div>');
        div.html('<span>Сделать ход</span>');
        div.on('click', function () {
            window._client.gameManager.sendTurn({'t': (new Date).getSeconds});
        });
        bdiv.append(div);

        div = $('<div>');
        div.html('<span>ход назад</span>');
        div.on('click', function () {
            window._client.gameManager.sendTakeBack();
        });
        bdiv.append(div);

        div = $('<div>');
        div.attr('id', 'time');
    }
};
