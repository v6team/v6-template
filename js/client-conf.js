var _clientConf = {
    game: 'test2',
    port: 8078,
    resultDialogDelay: 1000,
    reload: true,
    autoShowProfile: true,
    idleTimeout: 0,
    apiEnable: true,
    showSpectators: true,
    api: '//webdev.local:8081/'
};
