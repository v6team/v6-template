'use strict';
window.onload = function() {
    window.LogicGame.init(function(){
        var user = window._userData;
        var conf = window._clientConf;
        conf.game = user.game;
        window._client = new Client(conf).init(user);
        window._debugPanel();
    });
};